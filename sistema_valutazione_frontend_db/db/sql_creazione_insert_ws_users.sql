CREATE TABLE `test_lms2`.`ws_users` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `enabled` BOOLEAN NULL,
  `date_insert` DATETIME NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE);

INSERT INTO ws_users (id_user,username,password,enabled,date_insert)
VALUES (1,'Dante Alighieri','Password123',true,'2021-03-09'); 