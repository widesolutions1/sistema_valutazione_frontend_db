Use test_lms2;

CREATE VIEW ws_quiz (quiz_id, quiz_date, quiz_content, quiz_title, quiz_name, object_type, passing_grade)
AS SELECT p.ID, p.post_date_gmt, p.post_content, p.post_title, p.post_name, p.post_type, pm.meta_value
FROM test_lms2.wp_posts p, test_lms2.wp_postmeta pm
WHERE (p.post_type="stm-quizzes" AND p.post_status="publish") and (p.ID = pm.post_id and meta_key = 'passing_grade');

Create view  ws_lesson (lesson_id,lesson_date,lesson_content,lesson_title,lesson_name, object_type) as
SELECT ID,post_date_gmt,post_content,post_title,post_name, post_type
FROM test_lms2.wp_posts
where post_type="stm-lessons" and post_status="publish";

Create view  ws_assignment (assignment_id,assignment_date,assignment_content,assignment_title,assignment_name, object_type) as
SELECT ID,post_date_gmt,post_content,post_title,post_name, post_type
FROM test_lms2.wp_posts
where post_type="stm-assignments" and post_status="publish";


Create view  ws_course (course_id,course_date,course_content,course_title,course_name, meta_value, author) as
SELECT p.ID, p.post_date_gmt,p.post_content, p.post_title, p.post_name, n.meta_value, p.post_author
FROM test_lms2.wp_posts p
     LEFT join (SELECT m.post_id, m.meta_value
                FROM test_lms2.wp_postmeta m
                WHERE m.meta_key = 'curriculum') n on n.post_id = p.ID
WHERE p.post_type="stm-courses" and p.post_status="publish";

create view  ws_course_assignment (course_id,assignment_id,item_type) as
SELECT c.course_id,cb.item_id,cb.item_type
FROM test_lms2.ws_course c, test_lms2.wp_stm_lms_curriculum_bind cb
where c.course_id = cb.course_id and cb.item_type = "stm-assignments";

create view  ws_course_quiz (course_id,quiz_id,item_type) as
SELECT c.course_id,cb.item_id,cb.item_type
FROM test_lms2.ws_course c, test_lms2.wp_stm_lms_curriculum_bind cb
where c.course_id = cb.course_id and cb.item_type = "stm-quizzes";

create view  ws_course_lesson (course_id,lesson_id,item_type) as
SELECT c.course_id,cb.item_id,cb.item_type
FROM test_lms2.ws_course c, test_lms2.wp_stm_lms_curriculum_bind cb
where c.course_id = cb.course_id and cb.item_type = "stm-lessons";


create view  ws_student_course (student_id,course_id, author) as
SELECT uc.user_id,uc.course_id, c.author
FROM test_lms2.wp_stm_lms_user_courses uc, ws_course c
where uc.course_id = c.course_id;


create view  ws_instructor_course (instructor_id, course_id) as
SELECT c.author,uc.course_id
FROM test_lms2.wp_stm_lms_user_courses uc, ws_course c
where uc.course_id = c.course_id
group by c.course_id;

Create view  ws_student_assignment_points (student_id, course_id, assignment_id, score, assignment_timestamp, student_points_id, points_type) as
SELECT up.user_id,pm2.meta_value as course_id, pm1.meta_value as assignment_id,up.score, FROM_UNIXTIME(up.timestamp), up.user_points_id, up.action_id
FROM test_lms2.wp_stm_lms_user_points up, wp_postmeta pm1, wp_postmeta pm2
where up.action_id = "assignment_passed" and (pm1.meta_key = "assignment_id" and pm2.meta_key = "course_id") and (pm1.post_id = up.id and pm2.post_id = up.id);


CREATE VIEW ws_student_quiz_attempts (student_id, course_id, quiz_id, score, quiz_timestamp, student_points_id, points_type, num_passed, quiz_attempt) AS
    SELECT
        up.user_id AS user_id,
        cq.course_id AS course_id,
        up.id AS id,
        up.score AS score,
        FROM_UNIXTIME(up.timestamp) AS quiz_timestamp,
        up.user_points_id AS user_points_id,
        up.action_id AS action_id,
        IFNULL(SUM(ua.correct_answer), 0) AS num_passed,
        ua.attempt_number
    FROM
        ((test_lms2.wp_stm_lms_user_points up
        JOIN test_lms2.ws_course_quiz cq)
        JOIN test_lms2.wp_stm_lms_user_answers ua)
    WHERE
        ((up.action_id IN ('quiz_passed' , 'perfect_quiz'))
            AND (up.id = cq.quiz_id)
            AND (ua.user_id = up.user_id)
            AND (ua.course_id = cq.course_id)
            AND (up.id = ua.quiz_id))
    GROUP BY up.user_id , cq.course_id , up.id , up.score , FROM_UNIXTIME(up.timestamp) , up.user_points_id , up.action_id, ua.attempt_number;

Create view  ws_student_lesson_points (student_id, course_id, lesson_id, score, lesson_timestamp, student_points_id, points_type) as
SELECT up.user_id,cl.course_id, up.id,up.score, FROM_UNIXTIME(up.timestamp), up.user_points_id, up.action_id
FROM test_lms2.wp_stm_lms_user_points up, ws_course_lesson cl
where up.action_id = "lesson_passed" and up.id = cl.lesson_id;


Create view  ws_student_points (student_id, score) as
select student_id, sum(score) as score from (SELECT student_id, score AS score
    FROM ws_student_assignment_points
     union all
    SELECT student_id, score AS score
    FROM  ws_student_lesson_points
	union all
    SELECT student_id, score AS score
    FROM ws_student_quiz_attempts
) student_points
group by student_id;


CREATE VIEW `test_lms2`.`ws_learning_objects` (learning_object_id,learning_object_date,learning_object_content,learning_object_title,learning_object_name,learning_object_type, learning_object_author, learning_object_passing_grade) AS
    SELECT
        `p`.`ID` AS `ID`,
        `p`.`post_date_gmt` AS `post_date_gmt`,
        `p`.`post_content` AS `post_content`,
        `p`.`post_title` AS `post_title`,
        `p`.`post_name` AS `post_name`,
        `p`.`post_type` AS `post_type`,
        `p`.`post_author` AS `post_author`,
        `pm`.`meta_value` AS `meta_value`
    FROM
        (`test_lms2`.`wp_posts` `p`
        JOIN `test_lms2`.`wp_postmeta` `pm`)
    WHERE
        ((`p`.`post_type` = 'stm-quizzes')
            AND (`p`.`post_status` = 'publish')
            AND (`p`.`ID` = `pm`.`post_id`)
            AND (`pm`.`meta_key` = 'passing_grade'))
    UNION SELECT
        `test_lms2`.`wp_posts`.`ID` AS `ID`,
        `test_lms2`.`wp_posts`.`post_date_gmt` AS `post_date_gmt`,
        `test_lms2`.`wp_posts`.`post_content` AS `post_content`,
        `test_lms2`.`wp_posts`.`post_title` AS `post_title`,
        `test_lms2`.`wp_posts`.`post_name` AS `post_name`,
        `test_lms2`.`wp_posts`.`post_type` AS `post_type`,
        `test_lms2`.`wp_posts`.`post_author` AS `post_author`,
        NULL AS `meta_value`
    FROM
        `test_lms2`.`wp_posts`
    WHERE
        ((`test_lms2`.`wp_posts`.`post_type` IN ('stm-assignments' , 'stm-lessons'))
            AND (`test_lms2`.`wp_posts`.`post_status` = 'publish'));

Create view  ws_student (
	student_id,
    student_login,
    student_nicename,
    student_fullname,
    student_email,
    registration_date,
    display_name,
    student_role,
    num_courses,
    score,
    partial_assignments,
    total_assignments,
    partial_lessons,
    total_lessons,
    partial_quizzes,
    total_quizzes, avg_score_interview) as
select
	u.ID,
    u.user_login,
    u.user_nicename,
    CONCAT( um2.meta_value, " ", um3.meta_value ) AS student_fullname,
    u.user_email,
    u.user_registered,
    u.display_name,
    um.meta_value,
    IFNULL(c.num_courses, 0) num_courses,
    IFNULL(sp.score, 0) score,
    IFNULL(sap.partial_assignments,0) partial_assignments,
    IFNULL(ta.total_assignments,0) total_assignments,
    IFNULL(slp.partial_lessons,0) partial_lessons,
    IFNULL(tl.total_lessons,0) total_lessons,
    IFNULL(sqp.partial_quizzes,0) partial_quizzes,
    IFNULL(tq.total_quizzes,0) total_quizzes,
    null avg_score_interview
from
	test_lms2.wp_usermeta um,
    test_lms2.wp_usermeta um2,
    test_lms2.wp_usermeta um3,
    test_lms2.wp_users u
    left outer join
    (select
		sc.student_id,
        count(a.assignment_id) as total_assignments
	from test_lms2.ws_assignment a,
		test_lms2.ws_student_course sc,
		test_lms2.ws_course_assignment ca
    where (a.assignment_id = ca.assignment_id)
		and (ca.course_id = sc.course_id)
    group by sc.student_id) ta
    ON u.ID = ta.student_id
    left outer join
    (select
		student_id,
        score as score
	from test_lms2.ws_student_points
    group by student_id) sp
    ON u.ID = sp.student_id
    left outer join
    (select
		user_id,
        count(course_id) as num_courses
	from test_lms2.wp_stm_lms_user_courses
    group by user_id) c
    ON u.ID = c.user_id
    left outer join
    (select
		sc.student_id,
		count(l.lesson_id) as total_lessons
	from test_lms2.ws_lesson l,
		test_lms2.ws_student_course sc,
        test_lms2.ws_course_lesson cl
	where (l.lesson_id = cl.lesson_id)
		and (cl.course_id = sc.course_id)
	group by sc.student_id) tl
	ON u.ID = tl.student_id
    left outer join
    (select
		sc.student_id,
        count(q.quiz_id) as total_quizzes
	from test_lms2.ws_quiz q,
		test_lms2.ws_student_course sc,
        test_lms2.ws_course_quiz cq
	where (q.quiz_id = cq.quiz_id)
		and (cq.course_id = sc.course_id)
	group by sc.student_id) tq
    ON u.ID = tq.student_id
    left outer join
    (select
		student_id,
        count(assignment_id)  as partial_assignments
	from test_lms2.ws_student_assignment_points
    group by student_id) sap
    ON u.ID = sap.student_id
    left outer join
    (select
		student_id,
        count(lesson_id)  as partial_lessons
	from test_lms2.ws_student_lesson_points
    group by student_id) slp
    ON u.ID = slp.student_id
    left outer join
    (select
		student_id,
        count(quiz_id)  as partial_quizzes
	from test_lms2.ws_student_quiz_attempts
    group by student_id) sqp
    ON u.ID = sqp.student_id
    WHERE u.ID = um.user_id
    and um.meta_value = 'a:1:{s:10:"subscriber";b:1;}'
    and u.ID = um2.user_id
    and u.ID = um3.user_id
    and um2.meta_key = 'first_name'
    and um3.meta_key = 'last_name';


Create view  ws_instructor (instructor_id,instructor_login,instructor_nicename,instructor_email,registration_date,display_name,instructor_role,num_courses,num_students, num_assignments, num_lessons, num_quizzes) as
SELECT u.ID,u.user_login,u.user_nicename,u.user_email,u.user_registered,u.display_name, um.meta_value,IFNULL(c.num_courses, 0) num_courses, IFNULL(pc.num_students, 0) num_students, IFNULL(ac.num_assignments,0) num_assignments, IFNULL(lc.num_lessons,0) num_lessons, IFNULL(qc.num_quizzes,0) num_quizzes
from
	test_lms2.wp_usermeta um,
    test_lms2.wp_users u
    left outer join
    (select
        author, count(author)  as num_courses
	from test_lms2.ws_course
    group by author) c
    ON u.ID = c.author
    left outer join
    (select count(author) as num_students, author from (select count(student_id) as num_student, author from (
SELECT student_id, course_id, author
    FROM test_lms2.ws_student_course
     union
    SELECT student_id, course_id, author
    FROM test_lms2.ws_student_course
    where author = author
) student_course
group by student_id) partial_count) pc
    ON u.ID = pc.author
    left outer join
    (select count(learning_object_author) as num_assignments, learning_object_author from ws_learning_objects
    where learning_object_type = 'stm-assignments'
group by learning_object_author) ac
    ON u.ID = ac.learning_object_author
    left outer join
    (select count(learning_object_author) as num_lessons, learning_object_author from ws_learning_objects
    where learning_object_type = 'stm-lessons'
group by learning_object_author) lc
    ON u.ID = lc.learning_object_author
     left outer join
    (select count(learning_object_author) as num_quizzes, learning_object_author from ws_learning_objects
    where learning_object_type = 'stm-quizzes'
group by learning_object_author) qc
    ON u.ID = qc.learning_object_author
    where (u.ID = um.user_id and um.meta_key = 'wp_capabilities')
    and ((um.meta_value = 'a:1:{s:13:"administrator";b:1;}') or (um.meta_value = 'a:1:{s:18:"stm_lms_instructor";b:1;}'))
    group by u.ID;

CREATE

VIEW `test_lms2`.`ws_course_learning_object` (`course_id` , `learning_object_id` , `learning_object_type`) AS
    SELECT
        `c`.`course_id` AS `course_id`,
        `cb`.`item_id` AS `learning_object_id`,
        `cb`.`item_type` AS `learning_object_type`
    FROM
        (`test_lms2`.`ws_course` `c`
        JOIN `test_lms2`.`wp_stm_lms_curriculum_bind` `cb`)
    WHERE
        ((`c`.`course_id` = `cb`.`course_id`)
            AND (`cb`.`item_type` IN ('stm-assignments' , 'stm-lessons', 'stm-quizzes')));

CREATE

VIEW `test_lms2`.`ws_student_learning_object_points` (`student_id` , `course_id` , `learning_object_id` , `score` , `learning_object_timestamp` , `student_points_id` , `points_type` , `learning_object_type`) AS
    SELECT
        `up`.`user_id` AS `user_id`,
        `clo`.`course_id` AS `course_id`,
        `pm1`.`meta_value` AS `meta_value`,
        `up`.`score` AS `score`,
        FROM_UNIXTIME(`up`.`timestamp`) AS `learning_object_timestamp`,
        `up`.`user_points_id` AS `user_points_id`,
        `up`.`action_id` AS `action_id`,
        `clo`.`learning_object_type` AS `learning_object_type`
    FROM
        (((`test_lms2`.`wp_stm_lms_user_points` `up`
        JOIN `test_lms2`.`wp_postmeta` `pm1`)
        JOIN `test_lms2`.`wp_postmeta` `pm2`)
        JOIN `test_lms2`.`ws_course_learning_object` `clo`)
    WHERE
        ((`up`.`action_id` = 'assignment_passed')
            AND (`pm1`.`meta_key` = 'assignment_id')
            AND (`pm2`.`meta_key` = 'course_id')
            AND (`pm1`.`post_id` = `up`.`id`)
            AND (`pm2`.`post_id` = `up`.`id`)
            AND (`pm1`.`meta_key` = 'assignment_id')
            AND (`pm2`.`meta_key` = 'course_id')
            AND (`pm1`.`meta_value` = `clo`.`learning_object_id`)
            AND (`pm2`.`meta_value` = `clo`.`course_id`))
    UNION SELECT
        `up`.`user_id` AS `user_id`,
        `clo`.`course_id` AS `course_id`,
        `up`.`id` AS `id`,
        `up`.`score` AS `score`,
        FROM_UNIXTIME(`up`.`timestamp`) AS `learning_object_timestamp`,
        `up`.`user_points_id` AS `user_points_id`,
        `up`.`action_id` AS `action_id`,
        `clo`.`learning_object_type` AS `learning_object_type`
    FROM
        (`test_lms2`.`wp_stm_lms_user_points` `up`
        JOIN `test_lms2`.`ws_course_learning_object` `clo`)
    WHERE
        ((`up`.`action_id` IN ('lesson_passed' , 'quiz_passed', 'perfect_quiz'))
            AND (`up`.`id` = `clo`.`learning_object_id`))
    ORDER BY `user_id`;


    CREATE TABLE `test_lms2`.`ws_interview` (
          `interview_id` INT NOT NULL AUTO_INCREMENT,
          `interview_date` DATETIME NULL,
          `interview_content` LONGTEXT NULL,
          `interview_title` VARCHAR(45) NULL,
          `interview_course` VARCHAR(45) NULL,
          `instructor_id` INT NULL,
          PRIMARY KEY (`interview_id`));

   INSERT INTO ws_interview (interview_id,interview_date,interview_content,interview_title,interview_course,instructor_id)
   VALUES (1,'2021-03-09','Tutto ok','Intervista default','Java Back-End Developer',1);

          CREATE TABLE `test_lms2`.`ws_student_interview` (
          `interview_id` INT NOT NULL,
          `student_id` INT NOT NULL,
          `interview_vote` INT NULL,
          `interview_status` TINYINT NULL,
           `interview_opinion` LONGTEXT NULL,
          PRIMARY KEY (`interview_id`, `student_id`));

    INSERT INTO ws_student_interview (interview_id,student_id,interview_vote,interview_status,interview_opinion) VALUES (1,19,6,true,'Lo studente ha risposto correttamente alla domanda 3');
